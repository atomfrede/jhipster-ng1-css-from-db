package com.gitlab.atomfrede.jhipster.ng1.example.web.rest;

import com.gitlab.atomfrede.jhipster.ng1.example.CssfromdbApp;

import com.gitlab.atomfrede.jhipster.ng1.example.domain.Dummy;
import com.gitlab.atomfrede.jhipster.ng1.example.repository.DummyRepository;
import com.gitlab.atomfrede.jhipster.ng1.example.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DummyResource REST controller.
 *
 * @see DummyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CssfromdbApp.class)
public class DummyResourceIntTest {

    private static final String DEFAULT_CSS = "AAAAAAAAAA";
    private static final String UPDATED_CSS = "BBBBBBBBBB";

    @Autowired
    private DummyRepository dummyRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDummyMockMvc;

    private Dummy dummy;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DummyResource dummyResource = new DummyResource(dummyRepository);
        this.restDummyMockMvc = MockMvcBuilders.standaloneSetup(dummyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dummy createEntity(EntityManager em) {
        Dummy dummy = new Dummy()
            .css(DEFAULT_CSS);
        return dummy;
    }

    @Before
    public void initTest() {
        dummy = createEntity(em);
    }

    @Test
    @Transactional
    public void createDummy() throws Exception {
        int databaseSizeBeforeCreate = dummyRepository.findAll().size();

        // Create the Dummy
        restDummyMockMvc.perform(post("/api/dummies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dummy)))
            .andExpect(status().isCreated());

        // Validate the Dummy in the database
        List<Dummy> dummyList = dummyRepository.findAll();
        assertThat(dummyList).hasSize(databaseSizeBeforeCreate + 1);
        Dummy testDummy = dummyList.get(dummyList.size() - 1);
        assertThat(testDummy.getCss()).isEqualTo(DEFAULT_CSS);
    }

    @Test
    @Transactional
    public void createDummyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dummyRepository.findAll().size();

        // Create the Dummy with an existing ID
        dummy.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDummyMockMvc.perform(post("/api/dummies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dummy)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Dummy> dummyList = dummyRepository.findAll();
        assertThat(dummyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDummies() throws Exception {
        // Initialize the database
        dummyRepository.saveAndFlush(dummy);

        // Get all the dummyList
        restDummyMockMvc.perform(get("/api/dummies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dummy.getId().intValue())))
            .andExpect(jsonPath("$.[*].css").value(hasItem(DEFAULT_CSS.toString())));
    }

    @Test
    @Transactional
    public void getDummy() throws Exception {
        // Initialize the database
        dummyRepository.saveAndFlush(dummy);

        // Get the dummy
        restDummyMockMvc.perform(get("/api/dummies/{id}", dummy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dummy.getId().intValue()))
            .andExpect(jsonPath("$.css").value(DEFAULT_CSS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDummy() throws Exception {
        // Get the dummy
        restDummyMockMvc.perform(get("/api/dummies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDummy() throws Exception {
        // Initialize the database
        dummyRepository.saveAndFlush(dummy);
        int databaseSizeBeforeUpdate = dummyRepository.findAll().size();

        // Update the dummy
        Dummy updatedDummy = dummyRepository.findOne(dummy.getId());
        updatedDummy
            .css(UPDATED_CSS);

        restDummyMockMvc.perform(put("/api/dummies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDummy)))
            .andExpect(status().isOk());

        // Validate the Dummy in the database
        List<Dummy> dummyList = dummyRepository.findAll();
        assertThat(dummyList).hasSize(databaseSizeBeforeUpdate);
        Dummy testDummy = dummyList.get(dummyList.size() - 1);
        assertThat(testDummy.getCss()).isEqualTo(UPDATED_CSS);
    }

    @Test
    @Transactional
    public void updateNonExistingDummy() throws Exception {
        int databaseSizeBeforeUpdate = dummyRepository.findAll().size();

        // Create the Dummy

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDummyMockMvc.perform(put("/api/dummies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dummy)))
            .andExpect(status().isCreated());

        // Validate the Dummy in the database
        List<Dummy> dummyList = dummyRepository.findAll();
        assertThat(dummyList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDummy() throws Exception {
        // Initialize the database
        dummyRepository.saveAndFlush(dummy);
        int databaseSizeBeforeDelete = dummyRepository.findAll().size();

        // Get the dummy
        restDummyMockMvc.perform(delete("/api/dummies/{id}", dummy.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Dummy> dummyList = dummyRepository.findAll();
        assertThat(dummyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Dummy.class);
        Dummy dummy1 = new Dummy();
        dummy1.setId(1L);
        Dummy dummy2 = new Dummy();
        dummy2.setId(dummy1.getId());
        assertThat(dummy1).isEqualTo(dummy2);
        dummy2.setId(2L);
        assertThat(dummy1).isNotEqualTo(dummy2);
        dummy1.setId(null);
        assertThat(dummy1).isNotEqualTo(dummy2);
    }
}
