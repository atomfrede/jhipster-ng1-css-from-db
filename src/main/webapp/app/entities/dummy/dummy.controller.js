(function() {
    'use strict';

    angular
        .module('cssfromdbApp')
        .controller('DummyController', DummyController);

    DummyController.$inject = ['Dummy'];

    function DummyController(Dummy) {

        var vm = this;

        vm.dummies = [];

        loadAll();

        function loadAll() {
            Dummy.query(function(result) {
                vm.dummies = result;
                vm.searchQuery = null;
            });
        }
    }
})();
