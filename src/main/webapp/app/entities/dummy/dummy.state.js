(function() {
    'use strict';

    angular
        .module('cssfromdbApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('dummy', {
            parent: 'entity',
            url: '/dummy',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cssfromdbApp.dummy.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dummy/dummies.html',
                    controller: 'DummyController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dummy');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('dummy-detail', {
            parent: 'dummy',
            url: '/dummy/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'cssfromdbApp.dummy.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dummy/dummy-detail.html',
                    controller: 'DummyDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dummy');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Dummy', function($stateParams, Dummy) {
                    return Dummy.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'dummy',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('dummy-detail.edit', {
            parent: 'dummy-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dummy/dummy-dialog.html',
                    controller: 'DummyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Dummy', function(Dummy) {
                            return Dummy.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dummy.new', {
            parent: 'dummy',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dummy/dummy-dialog.html',
                    controller: 'DummyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                css: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('dummy', null, { reload: 'dummy' });
                }, function() {
                    $state.go('dummy');
                });
            }]
        })
        .state('dummy.edit', {
            parent: 'dummy',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dummy/dummy-dialog.html',
                    controller: 'DummyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Dummy', function(Dummy) {
                            return Dummy.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dummy', null, { reload: 'dummy' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dummy.delete', {
            parent: 'dummy',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dummy/dummy-delete-dialog.html',
                    controller: 'DummyDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Dummy', function(Dummy) {
                            return Dummy.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dummy', null, { reload: 'dummy' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
