(function() {
    'use strict';

    angular
        .module('cssfromdbApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
