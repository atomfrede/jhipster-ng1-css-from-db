package com.gitlab.atomfrede.jhipster.ng1.example.repository;

import com.gitlab.atomfrede.jhipster.ng1.example.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
