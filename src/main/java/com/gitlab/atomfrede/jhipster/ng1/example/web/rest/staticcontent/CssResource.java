package com.gitlab.atomfrede.jhipster.ng1.example.web.rest.staticcontent;

import com.codahale.metrics.annotation.Timed;
import com.gitlab.atomfrede.jhipster.ng1.example.service.util.RandomUtil;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/static-content")
public class CssResource {

    private Random random = new Random(42);

    @GetMapping(value = "/custom-css")
    @Timed
    public ResponseEntity<String> getCustomCss() {

        int value =random.nextInt(10 - 0 + 1) + 0;

        if (value >= 5) {
            return new ResponseEntity<>(green(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(red(), HttpStatus.OK);
        }


    }

    private String green() {

        return "body {\n" +
            "    background: green !important;\n" +
            "    font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n" +
            "    color: #333;\n" +
            "}";
    }

    private String red() {

        return "body {\n" +
            "    background: red !important;\n" +
            "    font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n" +
            "    color: #333;\n" +
            "}";
    }

}
