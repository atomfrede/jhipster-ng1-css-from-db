package com.gitlab.atomfrede.jhipster.ng1.example.repository;

import com.gitlab.atomfrede.jhipster.ng1.example.domain.Dummy;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Dummy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DummyRepository extends JpaRepository<Dummy,Long> {

}
