package com.gitlab.atomfrede.jhipster.ng1.example.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.gitlab.atomfrede.jhipster.ng1.example.domain.Dummy;

import com.gitlab.atomfrede.jhipster.ng1.example.repository.DummyRepository;
import com.gitlab.atomfrede.jhipster.ng1.example.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Dummy.
 */
@RestController
@RequestMapping("/api")
public class DummyResource {

    private final Logger log = LoggerFactory.getLogger(DummyResource.class);

    private static final String ENTITY_NAME = "dummy";

    private final DummyRepository dummyRepository;

    public DummyResource(DummyRepository dummyRepository) {
        this.dummyRepository = dummyRepository;
    }

    /**
     * POST  /dummies : Create a new dummy.
     *
     * @param dummy the dummy to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dummy, or with status 400 (Bad Request) if the dummy has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dummies")
    @Timed
    public ResponseEntity<Dummy> createDummy(@RequestBody Dummy dummy) throws URISyntaxException {
        log.debug("REST request to save Dummy : {}", dummy);
        if (dummy.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dummy cannot already have an ID")).body(null);
        }
        Dummy result = dummyRepository.save(dummy);
        return ResponseEntity.created(new URI("/api/dummies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dummies : Updates an existing dummy.
     *
     * @param dummy the dummy to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dummy,
     * or with status 400 (Bad Request) if the dummy is not valid,
     * or with status 500 (Internal Server Error) if the dummy couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dummies")
    @Timed
    public ResponseEntity<Dummy> updateDummy(@RequestBody Dummy dummy) throws URISyntaxException {
        log.debug("REST request to update Dummy : {}", dummy);
        if (dummy.getId() == null) {
            return createDummy(dummy);
        }
        Dummy result = dummyRepository.save(dummy);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dummy.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dummies : get all the dummies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dummies in body
     */
    @GetMapping("/dummies")
    @Timed
    public List<Dummy> getAllDummies() {
        log.debug("REST request to get all Dummies");
        return dummyRepository.findAll();
    }

    /**
     * GET  /dummies/:id : get the "id" dummy.
     *
     * @param id the id of the dummy to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dummy, or with status 404 (Not Found)
     */
    @GetMapping("/dummies/{id}")
    @Timed
    public ResponseEntity<Dummy> getDummy(@PathVariable Long id) {
        log.debug("REST request to get Dummy : {}", id);
        Dummy dummy = dummyRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dummy));
    }

    /**
     * DELETE  /dummies/:id : delete the "id" dummy.
     *
     * @param id the id of the dummy to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dummies/{id}")
    @Timed
    public ResponseEntity<Void> deleteDummy(@PathVariable Long id) {
        log.debug("REST request to delete Dummy : {}", id);
        dummyRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
