/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gitlab.atomfrede.jhipster.ng1.example.web.rest.vm;
