package com.gitlab.atomfrede.jhipster.ng1.example.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Dummy.
 */
@Entity
@Table(name = "dummy")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Dummy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "css")
    private String css;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCss() {
        return css;
    }

    public Dummy css(String css) {
        this.css = css;
        return this;
    }

    public void setCss(String css) {
        this.css = css;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dummy dummy = (Dummy) o;
        if (dummy.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dummy.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Dummy{" +
            "id=" + getId() +
            ", css='" + getCss() + "'" +
            "}";
    }
}
